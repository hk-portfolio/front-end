# Responsive Front-end project #

My first project was training for responsive front-end techniques. Site's target audience is elementary school students. Coding style was not object oriented or properly functional at this point. I did one quiz and a math assignment.
More information about the project can be found on my portfolio website.

## Files ##

* Quiz:  visa.html, js/visa.js, questions: /henrin. Doesn't work if run locally due CORS restrictions.
* Math exercise: matikka.html, js/matikka.js