/* global $ */

// Tekijä: Henri Kaustinen

$(document).ready(function() {

	var correctAnswers = 0, maxCorrect = 0, maxTasks = 5, taskNumber = 0;
	var checkNext = false;
	/* Antamalla arvon true näkee lukujonoon liittyvien muuttujien arvoja ja
	muodostetaanko lukujono kuten on tarkoitus. */
	var debug = false;

	// Arvotaan kokonaisluku muuttujien min ja max väliltä.
	function rnd(min, max) {
		return Math.floor(Math.random() * (max - min + 1) ) + min;
	}

	// Päivittää etenemispalkin
	function update_progressbar() {
		var progress_percent = taskNumber / maxTasks * 100;
		$(".progress-bar").attr("style", "width: " + progress_percent + "%");
	}

	/* Tämä on Aloita visa / Seuraava -painike. Tarkistetaan vastaukset tai näytetään
	loppuruutu, jos viimeinen tehtävä on tehty, tai aloitetaan seuraava tehtävä. */
	function next() {
		if (checkNext) {
			// Montako lukua oikein tässä lukujonossa.
			var correctNumbers = 0;
			$(".inputField").each(function() {
				$(".clearMath").attr("disabled", true);
				$(this).attr("disabled", true);
				if ($(this).val() === $(this).attr("data-correct")) {
					$(this).attr("style", "background-color: green; color: white;");
					correctNumbers++;
				} else {
					$(this).attr("style", "background-color: #ff3333; color: white;");
				}
			});
			if (correctNumbers === 0 ) {
				$("#otsikko").html("Et saanut yhtään oikein. Yritä tsempata!");
			} else if (correctNumbers === 1 ) {
				$("#otsikko").html("Sait oikein yhden luvun.");
			} else {
				$("#otsikko").html("Sait " + correctNumbers + " lukua oikein!");
			}
			correctAnswers += correctNumbers;
			checkNext = false;
			$(".nextMath").html("Seuraava <i class='fas fa-long-arrow-alt-right'></i>");

		} else if (taskNumber === maxTasks) {
			update_progressbar();
			ending();

		} else {
			if (taskNumber === 0) {
				$("#startScreen,#startMenu").addClass("d-none");
				$("#heading,.help,#progressBar").removeClass("d-none");
			}
			update_progressbar();
			taskNumber++;
			$("#taskNumber-col").removeClass("d-none");
			$("#taskNumber-col").html("<span id='questionNumber'>Tehtävä " +
				taskNumber + " / " + maxTasks + " </span>");
			newSequence();
		}
	}

	// Seuraava / Aloita visa -painike
	$(".nextMath").click(function() {
		next();
	});

	// Myös Enterillä pääsee seuraavaan tehtävään.
	$(document).on("keypress", function(press) {
		if (press.which === 13) {
			next();
		}
	});

	// Tyhjennä -painike tyhjentää syötekentät.
	$(".clearMath").click(function() {
		$(".inputField").each(function() {
			$(this).val("");
		});
	});

	// Lopeta -painike
	$(".quitMath").click(function() {
		ending();
	});

	/* Alkuvalikossa valitaan tehtävien lukujonojen määrä.
		Vastausvaihtoehdoille tehdään click-tapahtumat. */
	$(".answer").click(function() {
	$("#lista").children().each(function() {
			$(this).removeClass("chosen");
		});
		$(this).addClass("chosen");
		maxTasks = Number($(this).attr("data-count"));
	});

	/* Arvotaan värit lukujonoon. Funktio palauttaa vaalean ja tummemman taustavärin,
	sekä fontin värin. Kahdella ensimmäisellä muodostetaan syötekenttien liukuväri ja
	tummemmilla värillä vihjelukujen tausta. */
	function getColor() {
		var colorNumber = rnd(0, 6);
		var color1 = "",
			color2 = "",
			color3 = "black";
		switch (colorNumber) {
			case 0:
				color1 = "#55efc4";
				color2 = "#00b894";
				break;
			case 1:
				color1 = "#ff7675";
				color2 = "#d63031";
				color3 = "white";
				break;
			case 2:
				color1 = "#74b9ff";
				color2 = "#0984e3";
				color3 = "white";
				break;
			case 3:
				color1 = "#fefe7c";
				color2 = "#fdcb6e";
				break;
			case 4:
				color1 = "#fab1a0";
				color2 = "#e17055";
				break;
			case 5:
				color1 = "#a29bfe";
				color2 = "#6c5ce7";
				color3 = "white";
				break;
			default:
				color1 = "#81ecec";
				color2 = "#00cec9";
		}
		return [color1, color2, color3];
	}
	
	// Alustetaan arvot uutta tehtävää varten.
	function newSequence() {

    if (rnd(0, 4) < 3) {
      var increment = 1;
    } else {
      var increment = 2;
    }

	var firstNumber, hintNumber1, hintNumber2, seqLength;

	if (increment === 1) {
		// seqLength = Lukujonon pituus
		seqLength = rnd(4, 8);
		firstNumber = rnd(0, 11 - seqLength);
		var lastNumber = firstNumber + seqLength - 1;
		hintNumber1 = rnd(firstNumber, lastNumber);
		do {
			hintNumber2 = rnd(firstNumber, lastNumber);
		} while (hintNumber1 === hintNumber2);

	} else {
		seqLength = rnd(4, 6);
		firstNumber = rnd(0, (6 - seqLength) * increment);
		var numbers = [];
		var addNumber = firstNumber;
		for (var i = 0; i < seqLength; i++) {
			numbers.push(addNumber);
			addNumber += increment;
		}
		var hintIndex = rnd(0, numbers.length - 1);
		hintNumber1 = numbers[hintIndex];
		numbers.splice(hintIndex, 1);
		hintIndex = rnd(0, numbers.length - 1);
		hintNumber2 = numbers[hintIndex];
	}

		$(".nextMath").html("Valmis <i class='fas fa-check'></i>");
		$(".quitMath,.clearMath,#task-col").removeClass("d-none");
		$("#task-col").html("");

		// Onko lukujono käänteinen eli seuraava numero edellistä pienempi.
		var reversed = rnd(0, 1);

		if (debug) {
			$("#task-col").append("<p id='debug'>Askelkoko " + increment + ", pituus: " +
				seqLength + " Alue: " + firstNumber + " - " + lastNumber + ", vinkkinumerot: " +
				hintNumber1 + ", " + hintNumber2 + " käänteinen: " + reversed + " tttttttt: " +
				numbers + " oikeat vastaukset: " + correctAnswers + " tehtävät yht: " + maxTasks +
				"<br></p>");
		}

		var cell = "",
			[mainColor, hintColor, darkColor] = getColor(),
			number = firstNumber;

		for (var y = 0; y < seqLength; y++) {
			if (number === hintNumber1 || number === hintNumber2) {
				cell = "<label class='hintField mt-sm-3 mb-sm-3 shadow-lg' style='color:" + darkColor +
					";background-color:" + hintColor + ";'><b>" + number + "</b></label>";
			} else {
				cell = "<input type='number' class='inputField mt-sm-3 mb-sm-3 shadow-lg' min='0'\
				max='10' data-correct=" + number + " style='background-image: linear-gradient(" +
					hintColor + ", " + mainColor + ");'>";
				maxCorrect++;
			}
			if (reversed) {
				$("#task-col").prepend(cell);
			} else {
				$("#task-col").append(cell);
			}
			number += increment;
		}
		
		$("#otsikko").html("Täytä puuttuvat lukujonon luvut")
		$(".clearMath").attr("disabled", false);
		checkNext = true;
	}

	/* Lopetusruutu. Annetaan palaute käyttäjälle ja mahdollisuus aloittaa
		uudet tehtävät. */
	function ending() {
		$("#startScreen").html("");
		$("#progressbar,.quitMath,.clearMath,#heading,#task-col,.help").addClass("d-none");
		$("#startScreen,#startMenu").removeClass("d-none");
		$(".nextMath").html("Aloita uudestaan <i class='far fa-play-circle'></i>");

		var message = "", trophy_image = "";
		var percentage = correctAnswers / maxCorrect * 100;

		if (percentage <= 50 ) {
			message = "Ei mennyt ihan putkeen. Parempaa onnea ensi kerralla!";
			trophy_image = "oh_no.png";
		} else if (percentage <= 65) {
			message = "Kaipaat vielä harjoitusta. Yritä vielä uudelleen.";
			trophy_image = "neutral.png";
		} else if (percentage <= 80) {
			message = "Olet jo melko etevä!";
			trophy_image = "good.png";
		} else if (percentage <= 90) {
			message = "Hieno suoritus!";
			trophy_image = "second.png";
		} else {
			message = "Sait kaikki oikein! Onneksi olkoon!";
			trophy_image = "first.png";
		}

		$("#startScreen").html("<h2>Lukujonotehtävä päättyi</h2>");
		$("#startScreen").append("<p>Sait oikein " + correctAnswers + " lukua " +
			maxCorrect + ":stä.</p>");

		if (taskNumber === maxTasks) {
			$("#startScreen").append("<p><img src=img/henrin/" + trophy_image +
				" class='trophy' alt='palkinto'></p>");
			$("#startScreen").append("<p>" + message + "</p>");
		} else {
			$("#startScreen").append("<p>Lopetit tehtävän kesken. Voit saada\
				palkinnon, kun teet kaikki lukujonot loppuun asti.</p>");
		}

		$("#startScreen").append("<p>Jos haluat yrittää uudestaan, valitse, kuinka\
			monta lukujonoa haluat tehdä ja valitse <i>Aloita uudestaan</i>.</p>");

		correctAnswers = 0;
		taskNumber = 0;
		maxCorrect = 0;
		checkNext = false;
	}
});