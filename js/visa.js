/* global $ */

// Tekijä: Henri Kaustinen

$(document).ready(function() {

	var questionNr = 0,
		correctAnswers = 0,
		maxQuestions = 10,
		startNew = true,
		questions = [];

	// Arvotaan kokonaisluku min ja max väliltä.
	function rnd(min, max) {
		return Math.floor(Math.random() * (max - min + 1) ) + min;
	}

	// Alustetaan alkuarvot visan aloitukselle.
	function init() {
		questionNr = 0;
    	correctAnswers = 0;
		questions = [];
		startNew = false;
		var file = "henrin/question";
		for (var i = 1; i <= maxQuestions; i++) {
			questions.push(file + i + ".html");
		}
		$("#quit").removeClass("d-none");
		$("#next").html("Seuraava");
		$("#next").removeClass("d-none");
	}

	// Estetään valintaelementtien klikkaaminen vastauksen jälkeen.
	function block(element) {
		var parent = element.parent();
		parent.children().each(function() {
			$(this).removeClass("clickable");
			$(this).off("click");
		});
	}

	// Käyttäjän klikatessa vastausvaihtoehtoa tarkistetaan, onko se oikein vai väärin.
	function answering() {
		$("body").on("click", ".vastaus", function() {
			$("body").off("click");
			block($(this));
			if ($(this).attr("data-vastaus") === "1") {
				$(this).attr("style", "background-color: green; color: white;");
				correctAnswers++;
			} else {
				$(this).attr("style", "background-color: #ff3333; color: white;");
			}
		});
	}

	// Päivitä etenemispalkki.
	function update_progressbar() {
		var progress_percent = questionNr / maxQuestions * 100;
		$(".progress-bar").attr("style", "width: " + progress_percent + "%");
	}

	// Aloita / Seuraava -painike aloittaa visan tai esittää seuraavan kysymyksen.
	$("#next").click(function() {
		// Uuden visan aloitus
		if (startNew || questions.length === 0) {
			$("#instructions").addClass("d-none");
			$("#startImage").removeClass("d-sm-block");
			$("#progressBar").removeClass("d-none");
			init();
		}
		update_progressbar();
		questionNr++;
		var number = rnd(0, questions.length - 1);
		$("#question-col").load(questions[number]);
		$("#question-col").removeClass("d-none");
		$("#taskNumber-col").html("<span id='questionNumber'>Kysymys " +
			questionNr + " / " + maxQuestions + " </span>");
		questions.splice(number, 1);
		if (questions.length === 0) {
			$(this).addClass("d-none");
		} else {
			$(this).html("Seuraava <i class='fas fa-arrow-right'></i>");
		}
		answering();
	});

	/* Lopeta -painikkeella lopetetaan visa, annetaan loppupalaute, kuva
		palkintona. */
	$("#quit").click(function() {
		update_progressbar();

		if (correctAnswers) {
			$("#question-col").html("Vastasit oikein " + correctAnswers +
				":een kysymykseen " + maxQuestions + ":stä.<br>");
		} else {
			$("#question-col").html("Et saanut yhtään kysymystä oikein.<br>");
		}

		var trophy_image = "", message = "";

		if (correctAnswers <= 3 ) {
			message = "Ei mennyt ihan putkeen. Parempaa onnea ensi kerralla!";
			trophy_image = "oh_no.png";
		} else if (correctAnswers <= 4) {
			message = "Kaipaat vielä harjoitusta. Yritä vielä uudelleen.";
			trophy_image = "neutral.png";
		} else if (correctAnswers <= 6) {
			message = "Olet jo melko etevä!";
			trophy_image = "good.png";
		} else if (correctAnswers <= 8) {
			message = "Hieno suoritus!";
			trophy_image = "second.png";
		} else {
			message = "Sait kaikki oikein! Onneksi olkoon!";
			trophy_image = "first.png";
		}

		$("#question-col").append("<p><img src=img/henrin/" + trophy_image + " class='trophy' alt='palkinto'></p>");
		$("#question-col").append(message + "<br>Valitse <i>Uusi visa</i>, jos haluat yrittää uudelleen.");

		$(this).addClass("d-none");
		$("#next").removeClass("d-none");
		$("#next").html("Uusi visa <i class='far fa-play-circle'></i>");
		startNew = true;
	});
});
